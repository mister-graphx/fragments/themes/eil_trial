/** # __APP_NAME__

*/
var prodLikeEnvs = ['production', 'staging'];

global._SRC_DIR = "src/";
global._SRC_PATH = _BASE_PATH + _SRC_DIR;
global._BUILD_DIR = "public/";
global._BUILD_PATH = _BASE_PATH + _BUILD_DIR;
global._JS_DIR = "js/";
global._CSS_DIR = "css/";
global._IMG_DIR = "images/";
global._SASS_DIR = "scss/";
global._PAGES_DIR = "pages/";
global._DATAS_DIR = "datas/";
global._TEMPLATES_DIR = "templates/";
global._PROTOTYPES_DIR = "prototypes/";

module.exports = {
    mode: 'development',
    // File system
    BasePath: _BASE_PATH,
    SrcPath: _BASE_PATH + _SRC_DIR,
    BuildPath: _BASE_PATH + _BUILD_DIR,
    sassPath: _SRC_PATH + _SASS_DIR,
    cssPath: _SRC_PATH + _CSS_DIR,
    ImagePath: _SRC_PATH + _IMG_DIR,
    JsPath: _SRC_PATH + _JS_DIR,
    sass:{
        outputStyle: 'nested',
        // we use eyeglass for scss module loading
        // local include path are relative to _BASE_PATH
        // eg: ../../_FRAMEWORKS
        includePaths: [

        ]
    },
    sourcemaps:{
        path: ''
    },
    // Emailing - MJML
    mjml:{
      options: {
        keepComments: true,
        beautify: false,
        minify: false,
        validationLevel: 'soft',
        filePath: '.'
      }
    },
    nunjuks:{
        // templateExt: '.tpl',
    },
    bundleConfig: {
        dest: _BASE_PATH + _BUILD_DIR // Bundle destination
    },
    bundle: {
        'styles':{
            styles:[
                _SRC_PATH + _CSS_DIR + 'styles.css'
            ],
            options: {
                minCss: prodLikeEnvs,
                uglify: false,
                rev: false,
                map:false,
                pluginOptions: { // pass additional options to underlying gulp plugins. By default the options object is empty
                    'gulp-clean-css': {
                        keepBreaks: true,
                        keepSpecialComments: 0
                    }
                },
                watch: {
                  scripts: false, // do not watch for changes since these almost never change
                  styles: true
                }
            }
        }
    },
    copy: [
        {
            src: [
                _SRC_PATH + 'css/*.map'
            ],
            base: _SRC_PATH + 'css/' ,
            watch:true
        },
        {
            src: [
                _SRC_PATH + '*.html'
            ],
            base: _SRC_PATH  ,
            watch:true
        },
        {
            src: [
                _SRC_PATH + 'images/**/*.{jpg,jpeg,png,gif,svg}'
            ],
            base: _SRC_PATH  ,
            watch:true
        }
    ]
};
